const express = require('express');
const nodemailer = require('nodemailer');
const cors = require('cors');
const path = require('path');
const compression = require('compression');

const app = express();

app.use(compression());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.post('/api/form', (req, res) => {
	let data = req.body;
	let smtpTransport = nodemailer.createTransport({
		service: 'gmail',
		port: 465,
		auth: {
			user: 'trustybytes.sharan@gmail.com',
			pass: 'trustybytes'
		}
	});

	let mailOptions = {
		from: `${data.email}`,
		to: 'trustybytes.sharan@gmail.com',
		subject: `Message from ${data.fullname}`,
		html: `<h3>Name : </h3>
				<p>${data.fullname}</p>
				<br/>
			   <h3>Subject : <h3/>
   				<p>${data.subject}<p/>
				<br/>
				<h3>Query : <h3/>
				<p>${data.message}<p/>`
	};

	smtpTransport.sendMail(mailOptions, (error, response) => {
		if (error) {
			res.send(error);
		} else {
			res.send('success');
		}
	});

	smtpTransport.close();
});

//serve static assets in production

if (process.env.NODE_ENV === 'production') {
	//set static folder
	app.use(express.static('client/build'));

	app.get('*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
	});
}

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`server running on port ${PORT}`));
